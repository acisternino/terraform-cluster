#
# Global variables
#
# Copyright (c) 2017 Andrea Cisternino
# Licensed under the Apache License, version 2.0.

name = "hazel"

aws_region = "eu-central-1"

vpc_cidr = "10.27.0.0/16"

tags = {
  built-with   = "terraform"
  project      = "terraform-cluster"
  creator      = "anci"
  cluster-name = "hazel"
}
