/*
 * Nomad cluster with Terraform.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

provider "aws" {
  region = "${var.aws_region}"
}

##---- Data -----------------------------------------------

#data "aws_availability_zones" "all" {}

# Amazon Linux AMI
# For Ubuntu use: "ubuntu/images/hvm-ssd/ubuntu-xenial*"
# Owner: 099720109477
data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-2017*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

##---- Key Pairs ------------------------------------------

# We use the standard SSH key
resource "aws_key_pair" "deployer" {
  key_name   = "${var.name}-key"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

##---- VPC ------------------------------------------------

module "vpc" {
  source = "./vpc"

  name = "${var.name}"
  cidr = "10.27.0.0/16"

  tags = "${var.tags}"
}

##---- Linux Bastion Host ---------------------------------

module "bastion" {
  source = "./bastion"

  name     = "${var.name}"
  region   = "${var.aws_region}"
  ssh_cidr = "${var.ssh_cidr}"

  ami      = "${data.aws_ami.amazon_linux.id}"
  vpc_id   = "${module.vpc.vpc_id}"
  subnets  = "${slice(module.vpc.public_subnets, 0, 2)}" # only first two subnets
  key_name = "${aws_key_pair.deployer.key_name}"

  tags = "${var.tags}"
}
