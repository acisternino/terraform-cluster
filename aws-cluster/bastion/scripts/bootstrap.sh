#!/bin/bash
set -e

## Harden SSH

## Configure awslogs

## Assign EIP

EIP="${elastic_ip}"
REGION="${region}"

INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

IFS=, temp=($EIP)
ALLOCATION_ID="$${temp[0]}"

echo "ALLOCATION_ID: $ALLOCATION_ID"
aws ec2 associate-address --region $REGION --instance-id $INSTANCE_ID --allocation-id $ALLOCATION_ID --allow-reassociation

INSTANCE_IP=$(ifconfig -a | grep inet | awk '{ print $2 }' | sed 's/addr://g' | head -1)
ASSIGNED=$(aws ec2 describe-addresses --region $REGION --output text | grep $INSTANCE_IP | wc -l)
if [ "$ASSIGNED" -eq 1 ]; then
    echo "EIP successfully assigned"
else
    echo "Error assigning EIP"
fi
