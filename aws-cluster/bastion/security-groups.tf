/*
 * AWS Bastion Host Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

##---- Security Groups ------------------------------------

# SSH access from outside world
resource "aws_security_group" "bastion" {
  name        = "Restricted SSH"
  description = "SSH access open to my IP"
  vpc_id      = "${var.vpc_id}"

  # SSH access from my IP range
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }

  # Eventually add ports for web consoles

  # "ping" support
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["${var.ssh_cidr}"]
  }
  # Outgoing access open to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = "${merge(var.tags, map("Name", "${var.name}-ssh-sg"))}"
}
