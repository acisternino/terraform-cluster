/*
 * AWS VPC Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

# Give a name to the default routing table that comes with the VPC
resource "aws_default_route_table" "default" {
  default_route_table_id = "${aws_vpc.main.default_route_table_id}"

  tags = "${merge(var.tags, map("Name", "${var.name}-main-rt"))}"
}

##---- Public Routing -------------------------------------

# Routing table for routing traffic between the public subnets and the IGW.
# We can use a single RT because the egress rules points to the same IGW.

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"

  # The default route, mapping the VPC's CIDR block to "local", is created
  # implicitly and cannot be specified.

  route {
    # This route catches all remaining outgoing traffic and sends it
    # directly to the IGW.
    cidr_block = "0.0.0.0/0"

    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = "${merge(var.tags, map("Name", "${var.name}-public-rt"))}"
}

# Attach the route table to the subnets

resource "aws_route_table_association" "public-rta" {
  count = "${length(data.aws_availability_zones.all.names)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

##---- Private Routing ------------------------------------

# Routing tables for routing traffic between the private subnet and the NAT gateways.
# We need multiple RT because each subnet has its own NAT Gateway.

resource "aws_route_table" "private" {
  count = "${length(data.aws_availability_zones.all.names)}"

  vpc_id = "${aws_vpc.main.id}"

  # The default route, mapping the VPC's CIDR block to "local", is created
  # implicitly and cannot be specified.

  route {
    # This route catches all remaining outgoing traffic and sends it
    # directly to the NAT gateway of the corresponding public subnet.
    cidr_block = "0.0.0.0/0"

    nat_gateway_id = "${element(aws_nat_gateway.nat_gw.*.id, count.index)}"
  }
  tags = "${merge(var.tags, map("Name", "${var.name}-private-rt-${count.index}"))}"
}

# Attach each route table to its subnet

resource "aws_route_table_association" "private-rta" {
  count = "${length(data.aws_availability_zones.all.names)}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
}
