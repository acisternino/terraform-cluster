/*
 * AWS VPC Terraform module.
 *
 * Copyright (c) 2017 Andrea Cisternino
 * Licensed under the Apache License, version 2.0.
 */

##---- VPC ------------------------------------------------

resource "aws_vpc" "main" {
  cidr_block           = "${var.cidr}"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = "${merge(var.tags, map("Name", "${var.name}-vpc"))}"
}

##---- Internet Gateway -----------------------------------

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(var.tags, map("Name", "${var.name}-igw"))}"
}

##---- Subnets --------------------------------------------

# One public subnet per AZ
resource "aws_subnet" "public" {
  count = "${length(data.aws_availability_zones.all.names)}"

  vpc_id                  = "${aws_vpc.main.id}"
  availability_zone       = "${data.aws_availability_zones.all.names[count.index]}"
  cidr_block              = "${cidrsubnet(aws_vpc.main.cidr_block, 4, count.index + 8)}"
  map_public_ip_on_launch = true

  depends_on = ["aws_internet_gateway.igw"]

  tags = "${merge(var.tags, map("Name", "${var.name}-public-${count.index}"))}"
}

# One private subnet per AZ
resource "aws_subnet" "private" {
  count = "${length(data.aws_availability_zones.all.names)}"

  vpc_id            = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.all.names[count.index]}"
  cidr_block        = "${cidrsubnet(aws_vpc.main.cidr_block, 3, count.index)}"

  tags = "${merge(var.tags, map("Name", "${var.name}-private-${count.index}"))}"
}

##---- NAT Gateways ---------------------------------------

resource "aws_nat_gateway" "nat_gw" {
  # use length of private subnets
  count = "${length(data.aws_availability_zones.all.names)}"

  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
  allocation_id = "${element(aws_eip.nat_ip.*.id, count.index)}"
}

##---- Elastic IPs ----------------------------------------

# Public IPs for the NATs
resource "aws_eip" "nat_ip" {
  # use length of private subnets
  count = "${length(data.aws_availability_zones.all.names)}"

  vpc = true
}
